package cubs.hackathon.parking.repository;

import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingSpot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ParkingSpotRepositoryTest {

    @Autowired
    private ParkingSpotRepository parkingSpotRepository;

    @Autowired
    private LocationRepository locationRepository;

    // testing if one to many relation really works
    @Test
    public void testAddParkingSpotsToALocation() {
        Location location = new Location("Onisifor Ghibu", "Tora Iorga");
        locationRepository.save(location);
        List<Location> allLocations = locationRepository.findAll();
        assertThat(allLocations.size(), is(1));

        ParkingSpot firstParkingSpot = new ParkingSpot(1);
        ParkingSpot secondParkingSpot = new ParkingSpot(2);
        location.addParkingSpot(firstParkingSpot);
        location.addParkingSpot(secondParkingSpot);
        locationRepository.save(location);

        Optional<Location> optionalUpdatedLocation = locationRepository.findById(location.getAddress());
        assertThat(optionalUpdatedLocation.isPresent(), is(true));

        Location updatedLocation = optionalUpdatedLocation.get();
        List<ParkingSpot> parkingSpotsFromALocation = updatedLocation.getParkingSpots();
        assertThat(parkingSpotsFromALocation.size(), is(2));
    }

}