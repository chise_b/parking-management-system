package cubs.hackathon.parking.repository;

import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingSpot;
import cubs.hackathon.parking.model.ParkingUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ParkingUserRepositoryTest {

    @Autowired
    private ParkingUserRepository parkingUserRepository;

    @Autowired
    private ParkingSpotRepository parkingSpotRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Test
    public void testAddAndFindUser() {
        ParkingUser user = new ParkingUser("BH14ATC", "Chise", "Bogdan","password", 1);
        parkingUserRepository.save(user);
        Optional<ParkingUser> optionalUser = parkingUserRepository.findById(user.getCarNumber());
        assertThat(optionalUser.isPresent(), is(true));
    }

    @Test
    public void testUpdateUser() {
        ParkingUser user = new ParkingUser("BH14ATC", "Chise", "Bogdan","password", 1);
        parkingUserRepository.save(user);
        Optional<ParkingUser> foundOptionalUser = parkingUserRepository.findById(user.getCarNumber());
        assertThat(foundOptionalUser.isPresent(), is(true));

        ParkingUser foundUser = foundOptionalUser.get();
        assertThat(foundUser.getSeniority(), is(1));

        int newSeniority = 10;
        foundUser.setSeniority(newSeniority);
        parkingUserRepository.save(foundUser);
        Optional<ParkingUser> updatedOptionalUser = parkingUserRepository.findById(user.getCarNumber());
        assertThat(updatedOptionalUser.isPresent(), is(true));

        ParkingUser updatedUser = updatedOptionalUser.get();
        assertThat(updatedUser.getSeniority(), is(newSeniority));
    }

    @Test
    public void testNoExistingUser() {
        List<ParkingUser> listOfAllUsers = parkingUserRepository.findAll();
        assertThat(listOfAllUsers.size(), is(0));
    }

    @Test
    public void testDeleteUser() {
        testNoExistingUser();
        ParkingUser user = new ParkingUser("BH14ATC", "Chise", "Bogdan","password", 1);
        parkingUserRepository.save(user);
        List<ParkingUser> listOfAllUsers = parkingUserRepository.findAll();
        assertThat(listOfAllUsers.size(), is(1));

        parkingUserRepository.deleteById(user.getCarNumber());
        listOfAllUsers = parkingUserRepository.findAll();
        assertThat(listOfAllUsers.size(), is(0));
    }

    @Test
    public void testMatchParkingUserWithParkingSpot(){
        testNoExistingUser();
        ParkingUser user = new ParkingUser("BH14ATC", "Chise", "Bogdan","password", 1);
        parkingUserRepository.save(user);
        List<ParkingUser> listOfAllUsers = parkingUserRepository.findAll();
        assertThat(listOfAllUsers.size(), is(1));

        Location location = new Location("Onisifor Ghibu", "Tora Iorga");
        locationRepository.save(location);

        ParkingSpot firstParkingSpot = new ParkingSpot(1);
        ParkingSpot secondParkingSpot = new ParkingSpot(2);
        location.addParkingSpot(firstParkingSpot);
        location.addParkingSpot(secondParkingSpot);
        locationRepository.save(location);

        Location foundLocation = locationRepository.findById("Onisifor Ghibu").get();
        ParkingSpot parkingSpot =  foundLocation.getParkingSpots().get(0);
        parkingSpot.setParkingUser(user);
        parkingSpotRepository.save(parkingSpot);

        ParkingSpot updatedParkingSpot = foundLocation.getParkingSpots().get(0);
        assertThat(updatedParkingSpot.getParkingUser().getCarNumber(), is(user.getCarNumber()));


    }

}