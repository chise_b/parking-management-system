package cubs.hackathon.parking.repository;

import cubs.hackathon.parking.model.ParkingUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingUserRepository extends JpaRepository<ParkingUser, String> {
}
