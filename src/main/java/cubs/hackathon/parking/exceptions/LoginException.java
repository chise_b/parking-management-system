package cubs.hackathon.parking.exceptions;

public class LoginException extends Exception {

    public LoginException(String message) {
        super(message);
    }
}
