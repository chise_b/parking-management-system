package cubs.hackathon.parking.exceptions;

public class AlreadyExistingUserException extends Exception {

    public AlreadyExistingUserException(String message){
        super(message);
    }
}
