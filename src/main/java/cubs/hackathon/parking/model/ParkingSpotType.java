package cubs.hackathon.parking.model;

public enum ParkingSpotType {
    WORKER_ASSIGNED, DAILY_ASSIGNED
}
