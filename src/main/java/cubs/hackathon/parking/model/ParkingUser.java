package cubs.hackathon.parking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name = "ParkingUser")
@Table(name = "parkinguser")
public class ParkingUser {

    @Id
    private String carNumber;
    private String firstName;
    private String lastName;

    @JsonIgnore
    private String password;

    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private ParkingUserType parkingUserType;

    private Integer seniority;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "id_parking_spot")
    private ParkingSpot parkingSpot;

    public ParkingUser() {
    }

    public ParkingUser(String carNumber, String firstName, String lastName, String password, Integer seniority) {
        this.carNumber = carNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.seniority = seniority;
        this.password = password;
        this.parkingUserType = ParkingUserType.WORKER;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSeniority() {
        return seniority;
    }

    public void setSeniority(Integer seniority) {
        this.seniority = seniority;
    }

    public void setParkingSpot(ParkingSpot parkingSpot) {
        this.parkingSpot = parkingSpot;
    }

    @JsonIgnore
    public ParkingSpot getParkingSpot() {
        return parkingSpot;
    }

    public String getCarNumber() {
        return carNumber;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public void setParkingUserType(ParkingUserType parkingUserType) {
        this.parkingUserType = parkingUserType;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }
}
