package cubs.hackathon.parking.model;

public enum ParkingUserType {
    ADMIN, WORKER
}
