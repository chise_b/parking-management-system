package cubs.hackathon.parking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Location")
@Table(name = "location")
public class Location {

    @Id
    private String address;
    private String locationName;

    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    @JoinColumn(name = "location_id")
    private List<ParkingSpot> parkingSpots = new ArrayList<>();

    public Location(){}

    public Location(String address, String locationName) {
        this.address = address;
        this.locationName = locationName;
    }

    public void addParkingSpot(ParkingSpot parkingSpot) {
        parkingSpots.add(parkingSpot);
    }

    public void removeParkingSpot(ParkingSpot parkingSpot) {
        parkingSpots.add(parkingSpot);
    }

    public String getAddress() {
        return address;
    }

    public String getLocationName() {
        return locationName;
    }

    public List<ParkingSpot> getParkingSpots() {
        return parkingSpots;
    }
}
