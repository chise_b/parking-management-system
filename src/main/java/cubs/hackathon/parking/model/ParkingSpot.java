package cubs.hackathon.parking.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity(name = "ParkingSpot")
@Table(name = "parkingspot")
public class ParkingSpot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_parking_spot")
    private Integer idParkingSpot;

    private Integer spotNumber;

    private Boolean temporaryAvailable = false;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date startOccupyingDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date endOccupyingDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date startDateTemporaryAvailability;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date endDateTemporaryAvailability;

    private Boolean available = true;

    @Enumerated(EnumType.STRING)
    private ParkingSpotType parkingSpotType;

    @OneToOne(mappedBy = "parkingSpot", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, optional = false)
    private ParkingUser parkingUser;

    public ParkingSpot() {

    }

    public ParkingSpot(Integer spotNumber) {
        this.spotNumber = spotNumber;
    }

    public void setParkingUser(ParkingUser parkingUser) {
        if (parkingUser == null) {
            if (this.parkingUser != null) {
                this.parkingUser.setParkingSpot(null);
            }
        } else {
            parkingUser.setParkingSpot(this);
        }
        this.parkingUser = parkingUser;
    }

    public void setTemporaryAvailable(Boolean temporaryAvailable) {
        this.temporaryAvailable = temporaryAvailable;
    }

    public Integer getSpotNumber() {
        return spotNumber;
    }

    public Boolean getTemporaryAvailable() {
        return temporaryAvailable;
    }

    public ParkingSpotType getParkingSpotType() {
        return parkingSpotType;
    }

    public void setParkingSpotType(ParkingSpotType parkingSpotType) {
        this.parkingSpotType = parkingSpotType;
    }

    public ParkingUser getParkingUser() {
        return parkingUser;
    }

    public Date getStartOccupyingDate() {
        return startOccupyingDate;
    }

    public void setStartOccupyingDate(Date startOccupyingDate) {
        this.startOccupyingDate = startOccupyingDate;
    }

    public Date getEndOccupyingDate() {
        return endOccupyingDate;
    }

    public void setEndOccupyingDate(Date endOccupyingDate) {
        this.endOccupyingDate = endOccupyingDate;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getIdParkingSpot() {
        return idParkingSpot;
    }
}
