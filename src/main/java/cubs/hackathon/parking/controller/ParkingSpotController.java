package cubs.hackathon.parking.controller;

import cubs.hackathon.parking.controller.request.ReserveParkingSpotRequest;
import cubs.hackathon.parking.exceptions.ParkingSpotUnavailableException;
import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingSpot;
import cubs.hackathon.parking.model.ParkingUser;
import cubs.hackathon.parking.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/parkingspot")
public class ParkingSpotController {

    @Autowired
    private Service service;

    @GetMapping(value = "/details/{spotNumber}")
    public ResponseEntity<ParkingSpot> getSpecificParkingSpotDetails(@PathVariable Integer spotNumber, @RequestBody Location location) {
        return new ResponseEntity<>(service.getSpecificParkingSpotDetails(spotNumber, location), HttpStatus.OK);
    }

    /**
     * It is supposed that the location is saved in memory of the client when doing the request.
     *
     * @param spotNumber
     * @param reserveParkingSpotRequest
     * @return
     */
    @PostMapping(value = "/reserve/{spotNumber}")
    public ResponseEntity<ParkingSpot> reserveASpot(@PathVariable Integer spotNumber, @RequestBody ReserveParkingSpotRequest reserveParkingSpotRequest) {
        ParkingSpot reservedParkingSpot = null;
        try {
            reservedParkingSpot = service.reserveParkingSpot(
                    spotNumber, reserveParkingSpotRequest.getParkingUser(), reserveParkingSpotRequest.getLocation(),
                    reserveParkingSpotRequest.getStartOccupyingDate(), reserveParkingSpotRequest.getEndOccupyingDate());
            return new ResponseEntity<>(reservedParkingSpot, HttpStatus.OK);
        } catch (ParkingSpotUnavailableException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping(value = "unreserve")
    public ResponseEntity<ParkingSpot> unreserveASpot(@RequestBody ParkingUser parkingUser){
        return new ResponseEntity<>(service.unreserveAParkingSpot(parkingUser), HttpStatus.OK);
    }
}
