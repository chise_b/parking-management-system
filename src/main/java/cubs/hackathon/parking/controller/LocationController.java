package cubs.hackathon.parking.controller;

import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingSpot;
import cubs.hackathon.parking.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/location")
public class LocationController {

    @Autowired
    private Service service;

    @GetMapping(value = "/all")
    public ResponseEntity<List<Location>> getAllLocations() {
        return new ResponseEntity<>(service.getAllLocations(), HttpStatus.OK);
    }

    @GetMapping(value = "/spotsdetails")
    public ResponseEntity<List<ParkingSpot>> getAllParkingSporsDetailsFromALocation(@RequestBody Location location) {
        return new ResponseEntity<>(service.getAllParkingSpotDetailsFromALocation(location.getAddress()), HttpStatus.OK);
    }
}
