package cubs.hackathon.parking.controller;

import cubs.hackathon.parking.controller.request.LoginParkingUserRequest;
import cubs.hackathon.parking.exceptions.AlreadyExistingUserException;
import cubs.hackathon.parking.exceptions.LoginException;
import cubs.hackathon.parking.model.ParkingSpot;
import cubs.hackathon.parking.model.ParkingUser;
import cubs.hackathon.parking.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/user")
public class ParkingUserController {

    @Autowired
    private Service service;

    @PostMapping(value = "/register")
    public ResponseEntity<ParkingUser> registerWorker(@RequestBody ParkingUser parkingUser) {

        try {
            ParkingUser registerdParkingUser = service.registerWorker(parkingUser);
            return new ResponseEntity<>(parkingUser, HttpStatus.CREATED);
        } catch (AlreadyExistingUserException e) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping(value = "/login")
    public ResponseEntity<ParkingUser> loginWorker(@RequestBody LoginParkingUserRequest loginParkingUserRequest) {
        try {
            ParkingUser parkingUser = service.loginUser(loginParkingUserRequest);
            return new ResponseEntity<>(parkingUser, HttpStatus.OK);
        } catch (LoginException e) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping(value = "parkingspot/details")
    public ResponseEntity<ParkingSpot> getDetailsAboutUserParkingSpot(@RequestBody ParkingUser parkingUser){
        return new ResponseEntity<>(service.getSpecificParkingSpotDetailsForAUser(parkingUser), HttpStatus.OK);
    }

}
