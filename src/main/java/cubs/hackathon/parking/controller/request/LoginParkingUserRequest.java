package cubs.hackathon.parking.controller.request;

public class LoginParkingUserRequest {

    private String carNumber;
    private String password;

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
