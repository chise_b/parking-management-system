package cubs.hackathon.parking.controller.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingUser;

import java.util.Date;

public class ReserveParkingSpotRequest {

    private Location location;
    private ParkingUser parkingUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date startOccupyingDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date endOccupyingDate;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ParkingUser getParkingUser() {
        return parkingUser;
    }

    public void setParkingUser(ParkingUser parkingUser) {
        this.parkingUser = parkingUser;
    }

    public Date getStartOccupyingDate() {
        return startOccupyingDate;
    }

    public void setStartOccupyingDate(Date startOccupyingDate) {
        this.startOccupyingDate = startOccupyingDate;
    }

    public Date getEndOccupyingDate() {
        return endOccupyingDate;
    }

    public void setEndOccupyingDate(Date endOccupyingDate) {
        this.endOccupyingDate = endOccupyingDate;
    }
}
