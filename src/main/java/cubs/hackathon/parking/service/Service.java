package cubs.hackathon.parking.service;

import cubs.hackathon.parking.controller.request.LoginParkingUserRequest;
import cubs.hackathon.parking.exceptions.AlreadyExistingUserException;
import cubs.hackathon.parking.exceptions.LoginException;
import cubs.hackathon.parking.exceptions.ParkingSpotUnavailableException;
import cubs.hackathon.parking.model.Location;
import cubs.hackathon.parking.model.ParkingSpot;
import cubs.hackathon.parking.model.ParkingSpotType;
import cubs.hackathon.parking.model.ParkingUser;
import cubs.hackathon.parking.model.ParkingUserType;
import cubs.hackathon.parking.repository.LocationRepository;
import cubs.hackathon.parking.repository.ParkingSpotRepository;
import cubs.hackathon.parking.repository.ParkingUserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class Service {

    @Autowired
    private ParkingUserRepository parkingUserRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private ParkingSpotRepository parkingSpotRepository;

    public ParkingUser registerWorker(ParkingUser parkingUser) throws AlreadyExistingUserException {
        Optional<ParkingUser> optionalFoundParkingUser = parkingUserRepository.findById(parkingUser.getCarNumber());
        if (optionalFoundParkingUser.isPresent()) {
            throw new AlreadyExistingUserException("The user with the car number " + parkingUser.getCarNumber() + " already exists");
        }
        parkingUser.setParkingUserType(ParkingUserType.WORKER);

        return parkingUserRepository.save(parkingUser);
    }

    public ParkingUser loginUser(LoginParkingUserRequest loginParkingUserRequest) throws LoginException {
        Optional<ParkingUser> optionalSearchedUser = parkingUserRepository.findById(loginParkingUserRequest.getCarNumber());
        if (!optionalSearchedUser.isPresent()) {
            throw new LoginException("Wrong password or username");
        }
        ParkingUser searchedUser = optionalSearchedUser.get();
        if (!searchedUser.getPassword().equals(loginParkingUserRequest.getPassword())) {
            throw new LoginException("Wrong password or username");
        }

        return searchedUser;
    }

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public List<ParkingSpot> getAllParkingSpotDetailsFromALocation(String locationAddress) {
        Optional<Location> optionalLocation = locationRepository.findById(locationAddress);
        Location location = optionalLocation.get();

        return location.getParkingSpots();
    }

    public ParkingSpot getSpecificParkingSpotDetails(Integer spotNumber, Location location) {
        Location foundLocation = databaseFindLocation(location);
        List<ParkingSpot> parkingSpots = foundLocation.getParkingSpots();

        return findParkingSpotByNumber(spotNumber, parkingSpots);
    }

    private ParkingSpot findParkingSpotByNumber(Integer parkingSpotNumber, List<ParkingSpot> parkingSpots) {
        ParkingSpot searchedParkingSpot = null;
        for (ParkingSpot parkingSpot : parkingSpots) {
            if (parkingSpot.getSpotNumber().equals(parkingSpotNumber)) {
                searchedParkingSpot = parkingSpot;
            }
        }
        return searchedParkingSpot;
    }

    private Location databaseFindLocation(Location location) {
        Optional<Location> optionalLocation = locationRepository.findById(location.getAddress());
        return optionalLocation.get();
    }

    public ParkingSpot reserveParkingSpot(Integer spotNumber, ParkingUser parkingUser, Location location, Date startOccupyingDate, Date endOccupyingDate) throws ParkingSpotUnavailableException {
        Optional<ParkingUser> optionalEagerParkingUser = parkingUserRepository.findById(parkingUser.getCarNumber());
        ParkingUser eagerParkingUser = optionalEagerParkingUser.get();
        Location parkingLocation = databaseFindLocation(location);
        List<ParkingSpot> parkingSpots = parkingLocation.getParkingSpots();
        ParkingSpot parkingSpot = findParkingSpotByNumber(spotNumber, parkingSpots);
        if (parkingSpot.getAvailable()) {
            if(parkingSpot.getTemporaryAvailable()){
                //TODO implement temporary reserving spot
            }
            else{
                reserveParkingSpotForAWorker(startOccupyingDate, endOccupyingDate, eagerParkingUser, parkingSpot);
            }
        } else {
            throw new ParkingSpotUnavailableException("The parking spot with number " + spotNumber + " is taken.");
        }

        return parkingSpot;
    }

    private void reserveParkingSpotForAWorker(Date startOccupyingDate, Date endOccupyingDate, ParkingUser eagerParkingUser, ParkingSpot parkingSpot) {
        parkingSpot.setParkingUser(eagerParkingUser);
        parkingSpot.setStartOccupyingDate(startOccupyingDate);
        parkingSpot.setEndOccupyingDate(endOccupyingDate);
        parkingSpot.setAvailable(false);
        eagerParkingUser.setParkingSpot(parkingSpot);
        parkingUserRepository.save(eagerParkingUser);
        parkingSpotRepository.save(parkingSpot);
    }

    public ParkingSpot getSpecificParkingSpotDetailsForAUser(ParkingUser parkingUser) {
        ParkingUser searchedParkingUser = parkingUserRepository.findById(parkingUser.getCarNumber()).get();
        return searchedParkingUser.getParkingSpot();
    }

    public ParkingSpot unreserveAParkingSpot(ParkingUser parkingUser) {
        ParkingUser searchedParkingUser = parkingUserRepository.findById(parkingUser.getCarNumber()).get();
        ParkingSpot parkingSpot = searchedParkingUser.getParkingSpot();
        unlinkAParkingUserFromAParkingSpot(searchedParkingUser, parkingSpot);
        return parkingSpotRepository.findById(parkingSpot.getIdParkingSpot()).get();
    }

    private void unlinkAParkingUserFromAParkingSpot(ParkingUser searchedParkingUser, ParkingSpot parkingSpot) {
        parkingSpot.setParkingUser(null);
        parkingSpot.setStartOccupyingDate(null);
        parkingSpot.setEndOccupyingDate(null);
        parkingSpot.setAvailable(true);
        searchedParkingUser.setParkingSpot(null);
        parkingUserRepository.save(searchedParkingUser);
        parkingSpotRepository.save(parkingSpot);
    }

    @PostConstruct
    private void initDatabase() throws ParkingSpotUnavailableException {
        initUsers();
        initLocationsAndParkingSpots();
        initReservedSpot();
    }

    private Date createDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        return cal.getTime();
    }

    private void initReservedSpot() throws ParkingSpotUnavailableException {
        Location foundLocation = locationRepository.findById("str. Nicolae Iorga").get();
        ParkingUser parkingUser = parkingUserRepository.findById("BH14ABC").get();
        Integer spotNumber = 1;
        Date startOccupyingDate = createDate(2018, 6, 12);
        Date endOccupyingDate = createDate(2019, 6, 12);
        reserveParkingSpot(spotNumber, parkingUser, foundLocation, startOccupyingDate, endOccupyingDate);
    }

    private void initLocationsAndParkingSpots() {
        Location location = new Location("str. Nicolae Iorga", "Tora1");
        for (int parkingSpotNumber = 1; parkingSpotNumber <= 10; parkingSpotNumber++) {
            ParkingSpot parkingSpot = new ParkingSpot(parkingSpotNumber);
            if (parkingSpotNumber < 9) {
                parkingSpot.setParkingSpotType(ParkingSpotType.WORKER_ASSIGNED);
            } else {
                parkingSpot.setParkingSpotType(ParkingSpotType.DAILY_ASSIGNED);
            }
            location.addParkingSpot(parkingSpot);
        }
        locationRepository.save(location);
    }

    private void initUsers() {
        ParkingUser user = new ParkingUser("BH14ABC", "Chise", "Bogdan", "bogdan", 1);
        parkingUserRepository.save(user);
    }
}
